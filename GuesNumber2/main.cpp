#include <iostream>

int main() {
  int min = 0;
  int max = 64;
  int middle = (max + min) / 2;
  std::cout << "Загадайте число от 0 до 63.\n";
  std::string answer;
  
  while ((max - min) > 1){
    std::cout << "Ваше число больше? " << middle << "\n";
    std::cin >> answer;
    if (answer == "да"){
      min = middle;
      middle = (max + min) / 2;
    }
    if (answer == "нет"){
      max = middle;
      middle = (max + min) / 2;
    }
  }
  std::cout << "Ваше число: " << max << "\n";
 return 0;
}
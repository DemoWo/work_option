#include <iostream>

int main() {
  int money;
  int quinBill = 0;
  std::cout << "Ввведите нужную сумму для выдачи: ";
  std::cin >> money;

  if (money <= 150000){
    
    int quinBill1 = 0; // 5000 купюра
    int quinBill2 = 0; // 2000 купюра
    int quinBill3 = 0; // 1000 купюра
    int quinBill4 = 0; // 500 купюра
    int quinBill5 = 0; // 200 купюра
    int quinBill6 = 0; // 100 купюра
  
    if (money < 100 || money % 100 >= 1) {
        std::cout << "Некоректно введена сумма.\n";}
          
          quinBill1 = money / 5000;
          if (money >= 5000) {
            money = money - quinBill1 * 5000;
            std::cout << "Выдано " << quinBill1 << " по 5000\n";      
          }
            quinBill2 = money / 2000;
         if (money >= 2000 ){
            money = money - quinBill2 * 2000;
            std::cout << "Выдано " << quinBill2 << " по 2000\n";
         }
            quinBill3 = money / 1000;
          if (money >= 1000 || money < 1000){
            money = money - quinBill3 * 1000;
            std::cout << "Выдано " << quinBill3 << " по 1000\n";
          }
            quinBill4 = money / 500;
          if (money >= 500 || money < 500) {
            money = money - quinBill4 * 500;
            std::cout << "Выдано " << quinBill4 << " по 500\n";
          }       
            quinBill5 = money / 200;
          if (money >= 200 || money < 200){
            money = money - quinBill5 * 200;
            std::cout << "Выдано " << quinBill5 << " по 200\n";
          }
            quinBill6 = money / 100;
          if (money >= 100 || money < 100 ){
            money = money - quinBill6 * 100;
            std::cout << "Выдано " << quinBill6 << " по 100 \n";
          }

          quinBill = quinBill1 + quinBill2 + quinBill3 + quinBill4 + quinBill5 + quinBill6;
          std::cout << "Всего выдано купюр: " << quinBill << "\n";
  
          quinBill = (quinBill1 * 5000 + quinBill2 * 2000 + quinBill3 * 1000 + quinBill4 * 500 + quinBill5 * 200 + quinBill6 * 100);

          std::cout << "Всего выдано: " << quinBill << " руб.\n";
              
  }else {
    std::cout << "Превышен лимит для выдачи.\n";
  }
  return 0;
} 
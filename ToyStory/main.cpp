#include <iostream>
#include <cmath>

int main() {
  int x;
  std::cout << "Введите сторону кубика: \n";
  std::cin >> x;

  float y;
  std::cout << "Введите первый размер бруска: \n";
  std::cin >> y;

  float z;
  std::cout << "Введите второй размер бруска: \n";
  std::cin >> z;

  float cubes = std::floor(y / 5) * std::floor(z / 5) * std::floor(x / 5);
  std::cout << "Из этого бруска можно изготовить " << cubes << " кубиков.\n";

  int set;
  int i = 0;
  int packaging = 0;

 while (cubes - packaging > packaging) {
    set = (cubes * i) * i;
    packaging = (set / cubes) * i;
    i++;
  }
  std::cout << "Из них можно составить набор из " << packaging << " кубиков.\n";  
return 0;
}